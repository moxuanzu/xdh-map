import Scale from './src/scale'

Scale.install = function (Vue) {
  Vue.component(Scale.name, Scale)
}
export default Scale